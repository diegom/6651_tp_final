
from __future__ import division

import numpy as np
import scipy as sp

from functools import partial

from scipy.stats import uniform, norm
from matplotlib import pyplot as plt

class Class(object):
    def __init__(self, P, F):
        self.P = P
        self.F = F
        self.train_samples = F.rvs(10**4)
        self.support = np.linspace(F.ppf(1e-4)-1, F.ppf(1-1e-4)+1, 1000)
        self.real_pdf = F.pdf(self.support)

c1 = Class(0.4, uniform(2,10-2))
c2 = Class(0.6, norm(2,1))
classes = [c1, c2]
x = np.linspace(-2,11,1000)

def ejercicio_d():
    ks = [1, 10, 50, 100]
    hs = [0.01, 0.1, 0.5, 1]

    test_samples, test_labels = get_samples(10**2)

    print '*** Parzen ***'
    for h in hs:
        pdf1 = partial(parzen_estimation, samples=c1.train_samples, h=h)
        pdf2 = partial(parzen_estimation, samples=c2.train_samples, h=h)
        result_labels = bayes_classification(test_samples, c1.P, pdf1, c2.P, pdf2)
        errors = np.sum(test_labels != result_labels)
        print 'h: %.2f\terrors: %d' % (h, errors)
    print '*** KNN ***'
    for k in ks:
        pdf1 = partial(knn_estimation, samples=c1.train_samples, k=k)
        pdf2 = partial(knn_estimation, samples=c2.train_samples, k=k)
        result_labels = bayes_classification(test_samples, c1.P, pdf1, c2.P, pdf2)
        errors = np.sum(test_labels != result_labels)
        print 'k: %d\terrors: %d' % (k, errors)

def ejercicio_e():
    ks = [1, 11, 51]
    train_samples, train_labels = get_samples(2 * 10**4)
    test_samples, test_labels = get_samples(10**2)
    for k in ks:
        result_labels = knn_classification(test_samples, train_samples, train_labels, k)
        errors = np.sum(test_labels != result_labels)
        print 'k: %d\terrors: %d' % (k, errors)

def show_distributions():
    plt.plot(x, c1.F.pdf(x))
    plt.plot(x, c2.F.pdf(x))
    plt.title('Densidades de ambas clases')
    plt.ylabel('p(x)')
    plt.xlabel('variable aleatoria x')
    plt.legend(['$p(X|w_1)$ ~ Uniforme(2,10)', '$p(X|w_2)$ ~ Normal(2,1)'])
    plt.show()

def show_parzen_estimations():
    h = 0.7
    es1 = c1.P * parzen_estimation(x, c1.train_samples, h)
    es2 = c2.P * parzen_estimation(x, c2.train_samples, h)
    plt.plot(x, es1)
    plt.plot(x, es2)
    plt.title('Estimacion ventana de Parzen, $h=%.2f$' % h)
    plt.ylabel('p(x)')
    plt.xlabel('variable aleatoria x')
    plt.legend(['Estimacion $p(X|w_1)$ ~ Uniforme(2,10)', 'Estimacion$p(X|w_2)$ ~ Normal(2,1)'])
    plt.show()

def show_knn_estimations(k):
    es1 = c1.P * knn_estimation(x, c1.train_samples, k)
    es2 = c2.P * knn_estimation(x, c2.train_samples, k)
    plt.plot(x, es1)
    plt.plot(x, es2)
    plt.title('Estimacion KNN, $k=%d$' % k)
    plt.ylabel('p(x)')
    plt.xlabel('variable aleatoria x')
    plt.legend(['Estimacion $p(X|w_1)$ ~ Uniforme(2,10)', 'Estimacion $p(X|w_2)$ ~ Normal(2,1)'])
    plt.show()

def show_pdf():

    def pdf(x):
        return c1.P * c1.F.pdf(x) + c2.P * c2.F.pdf(x)

    plt.plot(x, pdf(x))
    plt.title('Densidad de X')
    plt.ylabel('p(x)')
    plt.xlabel('variable aleatoria x')
    plt.show()

def find_intersection():
    result = sp.optimize.minimize(lambda x: abs(c2.P * c2.F.pdf(x) - c1.P * c1.F.pdf(3)), 3)
    return result.x[0]

def bayes_error_bound():
    intersec = find_intersection()
    integral = sp.integrate.quad
    print integral(c2.F.pdf, intersec, 10)
    return c1.P * integral(c1.F.pdf, 2, intersec)[0] + \
           c2.P * integral(c2.F.pdf, intersec, 10)[0]

def distance(x, samples):
    n = len(samples)
    return np.abs((samples.reshape(n,1) - x.reshape(1,len(x))))

def parzen_estimation(x, samples, h):
    ''' Uniform window, centered at the origin '''
    n = len(samples)
    V = h**1
    return np.sum(distance(x, samples) / h <= 0.5, axis=0) / (n*V)

def knn_estimation(x, samples, k):
    n = len(samples)
    ds = distance(x, samples)
    ds.sort(axis=0)
    return (k/n) / (2 * ds[k,:])

def knn_classification(x, samples, labels, k):
    ds = distance(x, samples)
    index = ds.argsort(axis=0)
    bool_class = np.sum(labels[index[:k]] == 2, axis=0) > k / 2.
    return bool_class.astype(np.int8) + 1

def bayes_classification(x, p1, pdf1, p2, pdf2):
    bool_class = p1 * pdf1(x) < p2 * pdf2(x)
    return bool_class.astype(np.int8) + 1

def get_samples(n):
    c1, c2 = classes
    n1 = np.random.binomial(n, c1.P)
    n2 = n - n1

    s1 = c1.F.rvs(n1)
    s2 = c2.F.rvs(n2)
    l1 = np.ones(n1, dtype=np.int8)
    l2 = np.ones(n2, dtype=np.int8) + 1

    samples = np.hstack((s1,s2))
    labels = np.hstack((l1,l2))

    index = np.random.permutation(n)
    return samples[index], labels[index]

def best_h_lms(c):
    ''' Find the h that minimizes the square error between the real and
        estimated pdf of the given class c'''

    hs = np.logspace(-4,0,50) # 50 values of h logspace beetwen 10^-4 y 10^0
    errors = []
    for h in hs:
        estimated = parzen_estimation(c.support, c.train_samples, h)
        errors.append(np.sum((c.real_pdf-estimated)**2))

    return hs[np.argmin(errors)]
